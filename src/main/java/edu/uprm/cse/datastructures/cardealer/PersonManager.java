package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.model.PersonComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
@Path("/person")

public class PersonManager {

	private static CircularSortedDoublyLinkedList<Person> personList= new CircularSortedDoublyLinkedList<Person>(new PersonComparator());
	

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPerson(Person p){
		for(Person person : personList)
			if(person.getPersonId()==p.getPersonId())
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		
		personList.add(p);
		return Response.status(201).build();
	}
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Person getPerson(@PathParam("id") long id){
		for(int i=0; i<personList.size();i++){
			if(personList.get(i).getPersonId()==id){
				return personList.get(i);
			}
		}
		JsonError j= new JsonError("Error", "-> "+ id + " is an invalid Person ID");
		throw new NotFoundException(j);

	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Person[] getAllPeople(){
		Person[] people =new Person[personList.size()];

		for(int i=0; i<people.length;i++){
			people[i]=personList.get(i);

		}

		return people;
	}

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePerson(Person p){
		for(int i=0; i<personList.size();i++){
			if(personList.get(i).getPersonId()==p.getPersonId()){
				personList.remove(i);
				personList.add(p);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("{id}/delete")
	public Response deletePerson (@PathParam("id") long id){
		for(int i=0; i<personList.size(); i++){
			if(personList.get(i).getPersonId()==id){
				if(personList.remove(i)){
					return Response.status(Response.Status.OK).build();
				}

			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();

	}
	
	@GET
	@Path("/lastname/{lastname}")
	@Produces(MediaType.APPLICATION_JSON)
	public Person[] searchByLastName(@PathParam("lastName") String lastName){
		Person[] peeps = getAllPeople();
		int counter = 0;
		int rec = 0;
		
		for (int i = 0; i < peeps.length; i++) {
			if(peeps[i].getLastName().equals(lastName)) {
				counter++;
			}
		}
		
		Person[] peeps2 = new Person[counter];
		for (int i = 0; i < peeps.length; i++) {
			if(peeps[i].getLastName().equals(lastName)) {
				peeps2[rec++] = peeps[i];
			}
		}
		return peeps2;
	}
	public static CircularSortedDoublyLinkedList<Person> getPersonList() {
		// TODO Auto-generated method stub
		return personList;
	}


}