package edu.uprm.cse.datastructures.cardealer;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
@Path("/cars")
public class CarManager {

    private static CircularSortedDoublyLinkedList<Car> carList= new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	

	
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){ 
		
		for(Car newCar : carList)
			if(newCar.getCarId()==car.getCarId())
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		
		carList.add(car);
		return Response.status(201).build();
	}
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		for(int i=0; i<carList.size();i++){
			if(carList.get(i).getCarId()==id){
				return carList.get(i);
			}
		}
		JsonError j= new JsonError("Error", "Car " + id + " not found");
		throw new NotFoundException(j);
		
	}
	
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars(){
		Car[] cars=new Car[carList.size()];

		for(int i=0; i<cars.length;i++){
			cars[i]=carList.get(i);

		}

		return cars;
	}
	
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		for(int i=0; i<carList.size();i++){
			if(carList.get(i).getCarId()==car.getCarId()){
				carList.remove(i);
				carList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	@DELETE
	@Path("{id}/delete")
	public Response deleteCar (@PathParam("id") long id){
		for(int i=0; i<carList.size(); i++){
			if(carList.get(i).getCarId()==id){
				if(carList.remove(i)){
					return Response.status(Response.Status.OK).build();
				}
			
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
		
	}
	@Path("year/{carYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] searchByYear(@PathParam("carYear") int carYear){
		Car[] carL = this.getAllCars();
		int counter = 0;
		int index = 0;
		
		for (int i = 0; i < carL.length; i++) {
			if(carL[i].getCarYear() == carYear) {
				counter++;
			}
		}
		
		Car[] carL2 = new Car[counter];
		for (int i = 0; i < carL.length; i++) {
			if(carL[i].getCarYear() == carYear) {
				carL2[index++] = carL[i];
			}
		}
		return carL2;
	}
	@GET
	@Path("brand/{carBrand}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] searchByBrand(@PathParam("carBrand") String carBrand){
		Car[] carL = this.getAllCars();
		int counter = 0;
		int index = 0;
		
		for (int i = 0; i < carL.length; i++) {
			if(carL[i].getCarBrand().equals(carBrand)) {
				counter++;
			}
		}
		
		Car[] carL2 = new Car[counter];
		for (int i = 0; i < carL.length; i++) {
			if(carL[i].getCarBrand().equals(carBrand)) {
				carL2[index++] = carL[i];
			}
		}
		return carL2;
	}
	public static CircularSortedDoublyLinkedList<Car> getCarList() {
		// TODO Auto-generated method stub
		return carList;
	}

	
}
