package edu.uprm.cse.datastructures.cardealer.model;

import java.util.concurrent.atomic.AtomicLong;



public class CarUnit {
	

	private long carUnitId; // internal id of the unit
	private long carId; // id of the car object that represents the general for the car. 
                                           // This Car from project 1.
	private String VIN; // vehicle identification number
	private String color; // car color
	private String carPlate; // car plate (null until sold)
	private long personId; // id of the person who purchased the car. (null until 
                                                  //purchased)
	public CarUnit(long carUnitId, long carId, String VIN, String color, String carPlate, long personId){
		this.carUnitId=carUnitId;
		this.carId=carId;
		this.VIN=VIN;
		this.color=color;
		this.carPlate=carPlate;
		this.personId=personId;
	}
	public CarUnit(){}
	
	


	public long getCarUnitId(){
		return this.carUnitId;
	}

	public long getCarId() {
		return this.carId;
	}

	public String getVIN() {
		return this.VIN;
	}
	
	public String getColor(){
		return this.color;
	}

	public String getCarPlate(){
		return this.carPlate;
	}

	public long getPersonId() {
		return this.personId;
	}
	

	public void setCarUnitId(long carUnitId) {
		this.carUnitId = carUnitId;
	}
	public void setCarId(long carId) {
		this.carId = carId;
	}
	public void setVIN(String vIN) {
		VIN = vIN;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public void setCarPlate(String carPlate) {
		this.carPlate = carPlate;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}


	@Override
	public String toString(){
		return "Car Unit ID: " + carUnitId 
				+ " Car ID: " + carId
				+ " VIN: " + VIN + "\n"
				+"Color: "+color+ "\n"
				+ "Car Plate: " + carPlate + "\n"
				+ "Person Id: " + personId;

	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((VIN == null) ? 0 : VIN.hashCode());
		result = prime * result + (int) (carId ^ (carId >>> 32));
		result = prime * result + ((carPlate == null) ? 0 : carPlate.hashCode());
		result = prime * result + (int) (carUnitId ^ (carUnitId >>> 32));
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + (int) (personId ^ (personId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarUnit other = (CarUnit) obj;
		if (VIN == null) {
			if (other.VIN != null)
				return false;
		} else if (!VIN.equals(other.VIN))
			return false;
		if (carId != other.carId)
			return false;
		if (carPlate == null) {
			if (other.carPlate != null)
				return false;
		} else if (!carPlate.equals(other.carPlate))
			return false;
		if (carUnitId != other.carUnitId)
			return false;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (personId != other.personId)
			return false;
		return true;
	}



	
}