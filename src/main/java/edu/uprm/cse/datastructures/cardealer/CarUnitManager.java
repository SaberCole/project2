package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.model.CarUnitComparator;
import edu.uprm.cse.datastructures.cardealer.model.Person;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;



@Path("/carunit")
public class CarUnitManager {
	private static CircularSortedDoublyLinkedList<CarUnit> carUnitList = new CircularSortedDoublyLinkedList<CarUnit>(new CarUnitComparator());


	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public CarUnit[] getAllCarUnits() {
		CarUnit[] units =new CarUnit[carUnitList.size()];

		for(int i=0; i<units.length;i++){
			units[i]=carUnitList.get(i);

		}

		return units;
	}


	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public CarUnit getCarUnit(@PathParam("id") long id) {
		for (int i = 0; i < carUnitList.size(); i++) {
			if (carUnitList.get(i).getCarUnitId() == id)
				return carUnitList.get(i);
		}
		throw new NotFoundException(new JsonError("Error", "CarUnit " + id + " not found"));
	}


	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCarUnit(CarUnit carUnit) {
		CircularSortedDoublyLinkedList<Car> carList=CarManager.getCarList();
		CircularSortedDoublyLinkedList<Person>personList=PersonManager.getPersonList();
		boolean carFound=false,personFound=false;
		
		for(Car car:carList)
			if(carUnit.getCarId()==car.getCarId())
				carFound=true;
		
		if(!carFound)throw new NotFoundException(new JsonError("Error","Car: "+carUnit.getCarId()+" is not in the car List."));
		
		for(Person person:personList)
			if(carUnit.getPersonId()==person.getPersonId())
				personFound=true;
		
			if(!personFound)throw new NotFoundException(new JsonError("Error","Person: "+carUnit.getPersonId()+" is not in the Person List."));
			
		for(CarUnit newCar:carUnitList)
			if(newCar.getCarId()==carUnit.getCarId())
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		
		carUnitList.add(carUnit);
		return Response.status(201).build();
	}


	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCarUnit(CarUnit carUnit) {
		for (int i = 0; i < carUnitList.size(); i++) {
			if (carUnit.getCarUnitId() == carUnitList.get(i).getCarUnitId()) {
				carUnitList.remove(i);
				carUnitList.add(carUnit);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}


	@DELETE
	@Path("{id}/delete")
	public Response deleteCarUnit(@PathParam("id") long id) {
		for (int i = 0; i < carUnitList.size(); i++) {
			if (id == carUnitList.get(i).getCarUnitId()) {
				carUnitList.remove(carUnitList.get(i));
				return Response.status(Response.Status.OK).build();
			}
		}
		throw new NotFoundException(new JsonError("Error", "CarUnit " + id + " not found"));
	}


	public static CircularSortedDoublyLinkedList<CarUnit> getUnitList() {
		// TODO Auto-generated method stub
		return carUnitList;
	}
	
}