package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {
		
		String c1 = "";
		String c2 = "";
		
		c1 += o1.getCarBrand() + o1.getCarModel() + o1.getCarModelOption()+o1.getCarYear();
		c2 += o2.getCarBrand() + o2.getCarModel() + o2.getCarModelOption()+o2.getCarYear();
		
		return c1.compareTo(c2);
	}

}