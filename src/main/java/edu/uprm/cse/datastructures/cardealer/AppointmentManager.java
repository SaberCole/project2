package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Appointment;
import edu.uprm.cse.datastructures.cardealer.model.CarUnit;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.LinkedPositionalList;
import edu.uprm.cse.datastructures.cardealer.util.Position;

@Path("appointment")
public class AppointmentManager {
	private static final int SIZE=5;
	@SuppressWarnings("unchecked")
	private static LinkedPositionalList<Appointment>[] appointmentArray=new LinkedPositionalList[SIZE];

	static{
		for(int i=0; i<appointmentArray.length;i++){
			appointmentArray[i]=new LinkedPositionalList<Appointment>();
		}
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment[] getAllAppointmens(){
		Appointment[] appointments;
		int index=0;
		int arraySize=0;

		for(int i=0; i<appointmentArray.length;i++){
			arraySize+=appointmentArray[i].size();
		}
		appointments=new Appointment[arraySize];

		if(appointments.length>0){
			for(int i=0;i<appointmentArray.length;i++)
				for(Appointment myAppointment:appointmentArray[i])
					appointments[index++]=myAppointment;

		}
		return appointments;
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment getAppointment(@PathParam("id") long id){
		for(int i=0; i<appointmentArray.length;i++)
			for(Appointment myApp: appointmentArray[i])
				if(myApp.getAppointmentId()==id)
					return myApp;

		throw new NotFoundException(new JsonError("Error", "Appointment "+id+" not found"));
	}

	@GET
	@Path("/day/{day}")
	@Produces(MediaType.APPLICATION_JSON)
	public Appointment[] getAllAppointmentByDay(@PathParam("day") int day){
		Appointment[] apps;
		int index=0;
		if(day<0||day>=appointmentArray.length) throw new NotFoundException(new JsonError("Error","Day: "+day+" is invalid."));
		
		apps= new Appointment[appointmentArray[day].size()];

		if(apps.length>0)
			for(Appointment myApp: appointmentArray[day])
				apps[index++]=myApp;

		return apps;
	}
  
	/*@POST
	@Path("/add/{day}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response Appointment(@PathParam("day") String day, Appointment app){

		int dayNum;
		switch(day){
		case  "Monday":
			dayNum=0;
			break;

		case "Tuesday":
			dayNum=1;
			break;

		case "Wednesday":
			dayNum=2;
			break;
		case "Thursday":
			dayNum=3;
			break;
		case "Friday":
			dayNum=4;
			break;
		default:
			throw new NotFoundException(new JsonError("Error","Day: "+day+" is invalid."));
		}
		for(int i=0; i<appointmentArray.length;i++)
			for(Appointment myApp : appointmentArray[i])
				if(myApp.getAppointmentId()==app.getAppointmentId())
					return Response.status(Response.Status.NOT_ACCEPTABLE).build();

		appointmentArray[dayNum].addLast(app);
		return Response.status(201).build();
	}*/
	
	@POST
	@Path("/add/{day}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response Appointment(@PathParam("day") int day, Appointment app){

		
		if(day<0||day>=appointmentArray.length) throw new NotFoundException(new JsonError("Error","Day: "+day+" is invalid."));
		
		CircularSortedDoublyLinkedList<CarUnit> carUnitList=CarUnitManager.getUnitList();
		
		boolean found=false;
		
		for(CarUnit carUnit:carUnitList)
			if(app.getCarUnitId()==carUnit.getCarUnitId())
				found=true;
		if(!found) throw new NotFoundException(new JsonError("Error","CarUnit: "+app.getCarUnitId()+" is not in the car unit list."));
			
		
		for(int i=0; i<appointmentArray.length;i++)
			for(Appointment myApp : appointmentArray[i])
				if(myApp.getAppointmentId()==app.getAppointmentId())
					return Response.status(Response.Status.NOT_ACCEPTABLE).build();

		appointmentArray[day].addLast(app);
		return Response.status(201).build();
	}

	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateAppointment(Appointment app){

		Position<Appointment> current;

		for(int i=0;i<appointmentArray.length;i++){
			current=appointmentArray[i].first();

			while(current.getElement()!=null){
				if(current.getElement().getAppointmentId()==app.getAppointmentId()){
					appointmentArray[i].set(current, app);
					return Response.status(Response.Status.OK).build();
				}
				current=appointmentArray[i].after(current);
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@PUT
	@Path("{id}/update/day/{day}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateAppointmentByDay(@PathParam("day") int day, Appointment app){
		Position<Appointment> current;
		
		if(day<0||day>=appointmentArray.length) throw new NotFoundException(new JsonError("Error","Day: "+day+" is invalid."));
		
		for(int i=0; i<appointmentArray.length;i++){
			current=appointmentArray[i].first();

			while(current.getElement()!=null){
				if(current.getElement()!= null){
					appointmentArray[i].remove(current);
					appointmentArray[day].addLast(app);
					return Response.status(Response.Status.OK).build();
				}
				current=appointmentArray[i].after(current);
			}

		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("{id}/delete")
	public Response deletePerson(@PathParam("id") long id){

		Position<Appointment> current;
		for(int i=0; i<appointmentArray.length; i++){
			current=appointmentArray[i].first();
			while(current.getElement().getAppointmentId()==id){
				if(current.getElement().getAppointmentId()==id){
					appointmentArray[i].remove(current);
					return Response.status(Response.Status.OK).build();
				}
				current=appointmentArray[i].after(current);
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

}