package edu.uprm.cse.datastructures.cardealer.model;

import java.util.concurrent.atomic.AtomicLong;
public class Appointment {
	
	private long appointmentId; // internal id of the appointment
	private long carUnitId; // id of the car to be serviced
	private String job; // description of the job to be done (i.e.: “oil change”)
	private double bill; // cost of the service (initially 0).
	private static final AtomicLong counter = new AtomicLong(100);
	
	private Appointment(AppointmentBuilder appointmentBuilder){
		this.appointmentId = appointmentBuilder.appointmentId;
		this.carUnitId = appointmentBuilder.carUnitId;
		this.job = appointmentBuilder.job;
		this.bill = appointmentBuilder.bill;
		

	}
	public Appointment(){
		Appointment appointment = new Appointment.AppointmentBuilder().AppointmentId().build();
	    this.appointmentId=appointment.getAppointmentId();
		this.carUnitId = appointment.getCarUnitId();
		this.job = appointment.getJob();
		this.bill = appointment.getBill();
		
	}
	
	
	public Appointment(long appointmentId, long carUnitId, String job, double bill){
		Appointment appointment=new Appointment.AppointmentBuilder().AppointmentId()
				.Job(job)
				.Bill(bill)
				.build();
		
		this.appointmentId=appointment.getAppointmentId();
		this.carUnitId=appointment.getCarUnitId();
		this.job=appointment.getJob();
		this.bill=appointment.getBill();
				
		
	}
	
	public long getAppointmentId() {
		return appointmentId;
	}
	
	public long getCarUnitId() {
		return carUnitId;
	}
	
	public String getJob() {
		return job;
	}
		public double getBill() {
		return bill;
	}
		@Override
		public String toString(){
			return "Appointment Id: "+appointmentId
					+" Car Unit Id: "+carUnitId+"\n"
					+" Job:  "+job+"\n"
					+" Bill: "+bill;
		}
	
	public static class AppointmentBuilder{

		private long appointmentId; // internal id of the appointment
		private long carUnitId; // id of the car to be serviced
		private String job=""; // description of the job to be done (i.e.: “oil change”)
		private double bill;
		
		public AppointmentBuilder AppointmentId(){
			this.appointmentId=Appointment.counter.getAndIncrement();
			return this;
		}
		
		public AppointmentBuilder carUnitId(){
			this.carUnitId=Appointment.counter.getAndIncrement();
			return this;
		}
		
		public AppointmentBuilder Job(String job){
			this.job=job;
			return this;
		}
		public AppointmentBuilder Bill(double bill){
			this.bill=bill;
			return this;
		}
		
		public Appointment build(){
			return new Appointment(this);
		}
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (appointmentId ^ (appointmentId >>> 32));
		long temp;
		temp = Double.doubleToLongBits(bill);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (int) (carUnitId ^ (carUnitId >>> 32));
		result = prime * result + ((job == null) ? 0 : job.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appointment other = (Appointment) obj;
		if (appointmentId != other.appointmentId)
			return false;
		if (Double.doubleToLongBits(bill) != Double.doubleToLongBits(other.bill))
			return false;
		if (carUnitId != other.carUnitId)
			return false;
		if (job == null) {
			if (other.job != null)
				return false;
		} else if (!job.equals(other.job))
			return false;
		return true;
	}

	
	
	
}