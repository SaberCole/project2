package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarUnitComparator implements Comparator<CarUnit> {
@Override
public int compare(CarUnit o1, CarUnit o2){
	String c1="";
	String c2="";
	c1+=o1.getVIN();
	c2+=o1.getVIN();
	
	return c1.compareTo(c2);
}
}
